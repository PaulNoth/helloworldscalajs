import utest._
import org.scalajs.jquery._

object HelloWorldTest extends TestSuite {

  HelloWorld.setupUI()

  def tests = TestSuite {
    'HelloWorld {
      assert(jQuery("p:contains('Hello World')").length == 1)
    }

    'ButtonClick {
      def messageCount() = {
        jQuery("p:contains('You clicked a button!')").length
      }

      assert(messageCount() == 0)
      val button = jQuery("button:contains('Click Me')")
      assert(button.length == 1)

      val clicks = 5
      (1 to clicks).foreach {
        i =>
          button.click()
      }

      assert(messageCount() == clicks)
    }
  }
}
