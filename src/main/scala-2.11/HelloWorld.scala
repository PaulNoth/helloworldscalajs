import org.scalajs.jquery.jQuery

import scala.scalajs.js.JSApp
import scala.scalajs.js.annotation.JSExport

object HelloWorld extends JSApp {
  def main(): Unit = {
    jQuery(setupUI _)
    //appendPar(dom.document.body, "Hello World")
  }

  //def appendPar(targetNode: dom.Node, text: String): Unit = {
  //  val p = document.createElement("p")
  //  val textNode = document.createTextNode(text)
  //  p.appendChild(textNode)
  //  targetNode.appendChild(p)
  //}

  //@JSExport
  def addClickedMessage(): Unit = {
    //appendPar(dom.document.body, "You clicked a button!")
    jQuery("body").append("<p>You clicked a button!</p>")
  }

  def setupUI(): Unit = {
    jQuery("""<button type="button">Click Me</button>""").click(addClickedMessage _).appendTo(jQuery("body"))
    //jQuery("#clickbut").click(addClickedMessage _)
    jQuery("body").append("<p>Hello World</p>")
  }
}
