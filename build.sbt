enablePlugins(ScalaJSPlugin)

scalaJSStage in Global := FullOptStage

name := "HelloWorldScalaJS"

version := "1.0"

scalaVersion := "2.11.4"

skip in packageJSDependencies := false

//libraryDependencies += "org.scala-js" %%% "scalajs-dom" % "0.8.0"
libraryDependencies += "be.doeraene" %%% "scalajs-jquery" % "0.8.0"
libraryDependencies += "com.lihaoyi" %%% "utest" % "0.3.0" % "test"

jsDependencies += RuntimeDOM

testFrameworks += new TestFramework("utest.runner.Framework")

persistLauncher in Compile := true
persistLauncher in Test := false